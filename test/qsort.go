package main

import "fmt"

func main() {
	arr := []int{34,353,3434,234,2343434,78,5657,5676,788,345,1,1,33,2,0,3,454,3545,3676,8784,5667,5684}
	myqsort3(arr, 0, len(arr)-1)
	for _,num := range arr {
		fmt.Print(num, " ")
	}
	fmt.Println()
}

func myqsort12(arr []int, l, r int) {
	if l >= r {
		return
	}
	//pos := mypartition1(arr, l, r)
	pos := mypartition2(arr, l, r)
	myqsort12(arr, l, pos - 1)
	myqsort12(arr, pos + 1, r)
}

func myqsort3(arr []int, l, r int) {
	if l >= r {
		return
	}
	pos1, pos2 := mypartition3(arr, l, r)
	myqsort3(arr, l, pos1)
	myqsort3(arr, pos2, r)
}

//单路快排
func mypartition1(arr []int, l, r int) int {
	ppoint, i := arr[l], l
	for j := l + 1; j <= r; j++ {
		if arr[j] < ppoint {
			i++
			myswap(arr, i, j)
		}
	}
	myswap(arr, l, i)
	return i
}

//双路快排
func mypartition2(arr []int, l, r int) int {
	ppoint, i, j := arr[l], l + 1, r
	for true {
		for arr[i] < ppoint && i <= j {
			i++
		}
		for arr[j] > ppoint && j >= i {
			j--
		}
		if i > j {
			break
		}
		myswap(arr, i, j)
		i++
		j--
	}
	myswap(arr, l, j)
	return j
}

//三路快排
func mypartition3(arr []int, l, r int) (int, int) {
	ppoint, i := arr[l], l + 1
	lt, rt := l, r + 1
	for i < rt {
		if arr[i] < ppoint {
			lt++
			myswap(arr, lt, i)
			i++
		} else if arr[i] > ppoint {
			rt--
			myswap(arr, rt, i)
		} else {
			i++
		}
	}
	myswap(arr, lt, l)
	return lt - 1, rt
}

func myswap(arr []int, i, j int) {
	arr[i], arr[j] = arr[j], arr[i]
}
