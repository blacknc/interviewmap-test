//测试链表
package main

import (
    "interviewmap-test/src/linklist"
    "fmt"
)

func main() {
    llist := linklist.NewLinkList()
    for i := 0; i < 10; i++ {
        llist.Append(i)
    }
    for i := 0; i < 10; i++ {
        llist.Insert(i, 0)
    }
    for i := 0; i < 10; i++ {
        llist.Lpush(i)
    }
    for i := 0; i < 10; i++ {
        llist.Rpush(i)
    }
    fmt.Println(llist)

    node, _ := llist.Index(3)
    fmt.Printf("索引为3的节点：%d\n", node.Value())

    value := node.Value()
    node, _ = llist.Find(value)
    fmt.Printf("值为%d的节点：%d\n", value, node.Value())

    for i := 0; i < 10; i++ {
        node, _ := llist.Lpop()
        fmt.Println(node)
    }
    fmt.Println(llist)

    for i := 0; i < 10; i++ {
        node, _ := llist.Rpop()
        fmt.Println(node)
    }
    fmt.Println(llist)

    llist.RemoveIndex(5)
    fmt.Println("删除索引为5的节点：", llist)

    llist.RemoveValue(5)
    fmt.Println("删除值为5的节点：  ", llist)

    llist.Reverse()
    fmt.Println("反转链表：", llist)

    size := llist.Size()
    for i := 0; i < size; i++ {
        node, _ := llist.Lpop()
        fmt.Println(node)
    }
    fmt.Println("弹出全部节点", llist)
}
