//测试BST
package main

import (
	"interviewmap-test/src/tree"
)

func main() {
	bst := tree.NewBST()
	for i := 0; i < 10; i++ {
		bst.AddNode(i)
	}
	bst.PreTraversal()
}
