//二分搜索树
package tree

import test "fmt"

type Node struct {
	value       int
	left, right *Node
}

func NewNode(value int, left, right *Node) *Node {
	return &Node{value, left, right}
}

type BST struct {
	root *Node
	size int
}

func NewBST() *BST {
	return new(BST)
}

func (b *BST) GetSize() int {
	return b.size
}

func (b *BST) IsEmpty() bool {
	return b.size == 0
}

//添加节点
func (b *BST) AddNode(value int) {
	b.root = b._addChild(b.root, value)
}

//真正添加节点
func (b *BST) _addChild(root *Node, value int) *Node {
	if root == nil {
		b.size++
		return NewNode(value, nil, nil)
	}

	if value < root.value {
		root.left = b._addChild(root.left, value)
	} else if value > root.value {
		root.right = b._addChild(root.right, value)
	}
	return root
}

//先续遍历
func (b *BST) PreTraversal() {
	b._pre(b.root)
	test.Println()
}

//真正先续遍历
func (b *BST) _pre(root *Node) {
	if root != nil {
		test.Print(root.value, " ")
		b._pre(root.left)
		b._pre(root.right)
	}
}
