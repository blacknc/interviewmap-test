//有头结点的单向链表
package linklist

import "fmt"

type Node struct {
    value int
    next *Node
}

func NewNode(value int, next *Node) *Node {
    return &Node{value, next}
}

func (node *Node)String() string {
    return fmt.Sprintf("%d", node.value)
}

func (node *Node)Value() int {
    return node.value
}

type LinkList struct {
    size int
    root *Node
}

func NewLinkList() *LinkList {
    return &LinkList{0, NewNode(0, nil)}
}

//在指定索引位置插入节点
func (llist *LinkList)Insert(value int, index int) (*Node, error) {
    if index < 0 || index > llist.size {
        return nil, fmt.Errorf("非法索引：%d", index)
    }

    currNode := llist.root
    for i := 0; i < index; i++ {
        currNode = currNode.next
    }
    currNode.next = NewNode(value, currNode.next)
    llist.size++
    return currNode.next, nil
}

//在最后追加节点
func (llist *LinkList)Append(value int) (*Node, error) {
    return llist.Insert(value, llist.size)
}

//根据索引查找节点
func (llist *LinkList)Index(index int) (*Node, error) {
    if index < 0 || index >= llist.size {
        return nil, fmt.Errorf("非法索引：%d", index)
    }

    currNode := llist.root
    for i := 0; i <= index; i++ {
        currNode = currNode.next
    }
    return currNode, nil
}

//根据值查找节点
func (llist *LinkList)Find(value int) (*Node, error) {
    for currNode := llist.root.next; currNode != nil; currNode = currNode.next {
        if currNode.value == value {
            return currNode, nil
        }
    }
    return nil, fmt.Errorf("未找到值：%d", value)
}

//根据值删除节点
func (llist *LinkList)RemoveValue(value int) (*Node, error) {
    for prevNode, currNode := llist.root, llist.root.next; currNode != nil; prevNode, currNode = currNode, currNode.next {
        if currNode.value == value {
            prevNode.next = currNode.next
            currNode.next = nil
            llist.size--
            return currNode, nil
        }
    }
    return nil, fmt.Errorf("未找到值：%d", value)
}

//根据索引删除节点
func (llist *LinkList)RemoveIndex(index int) (*Node, error) {
    if index < 0 || index >= llist.size {
        return nil, fmt.Errorf("非法索引：%d", index)
    }

    prevNode, currNode := llist.root, llist.root.next
    for i := 1; i <= index; i++ {
        prevNode, currNode = currNode, currNode.next
    }
    prevNode.next = currNode.next
    currNode.next = nil
    llist.size--
    return currNode, nil
}

//左侧入队
func (llist *LinkList)Lpush(value int) (*Node, error) {
    return llist.Insert(value, 0)
}

//右侧出队
func (llist *LinkList)Rpop() (*Node, error) {
    return llist.RemoveIndex(llist.size-1)
}

//右侧入队
func (llist *LinkList)Rpush(value int) (*Node, error) {
    return llist.Insert(value, llist.size)
}

//左侧出队
func (llist *LinkList)Lpop() (*Node, error) {
    return llist.RemoveIndex(0)
}

//获取链表长度
func (llist *LinkList)Size() int {
    return llist.size
}

//将链表转字符串
func (llist *LinkList)String() string {
    output := "["
    for currNode := llist.root.next; currNode != nil; currNode = currNode.next {
        if currNode.next != nil {
            output += fmt.Sprintf("%d ", currNode.value)
        } else {
            output += fmt.Sprintf("%d", currNode.value)
        }
    }
    output += "]"
    return output
}

//将链表反转
func (llist *LinkList)Reverse() {
    currNode := llist.root.next
    llist.root.next = nil
    for currNode != nil {
        nextNode := currNode.next
        currNode.next = llist.root.next
        llist.root.next = currNode
        currNode = nextNode
    }
}
